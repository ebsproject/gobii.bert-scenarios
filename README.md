# Bert

Bert is a utility for testing the backend of the Gobii Project Service.

## Building and Running Bert

### Requirements

Ensure you have both [Maven](https://maven.apache.org/index.html) and [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) installed to your machine.

### Bert

#### Clone the Bert Repository
```bash
git clone http://gobiin1.bti.cornell.edu:6083/scm/gm/bert.git
```

#### Building Bert
```bash
cd bert
mvn clean install
cd ..
```

### Scenarios

#### Clone Scenario Repository
```bash
git clone http://gobiin1.bti.cornell.edu:6083/scm/gm/bert-scenarios.git
```

#### Building Bert Scenarios
```bash
mvn clean install
```

### Run Bert
```bash
cd bert-scenarios
./run.sh -web_host cbsugobiixvm.biohpc.cornell.edu \
         -web_port 8081 \
         -web_user gadm
         -web_pass ... \
         -crop dev \
         -db_host cbsugobiixvm.biohpc.cornell.edu \
         -db_port 5433 \
         -db_name gobii_dev \
         -db_user appuser \
         -db_pass ... \
         -ssh_host cbsugobiixvm.biohpc.cornell.edu:2222 \
         -ssh_user gadm \
         -ssh_pass ... \
         -suite legacy \
```
####  Some notes on the parameters:

These are common examples. Some may be applicable to your case, many will not.

The ssh host should target the compute node if using the docker network

The suite is a regex as defined by java. Multiple suites can be selected this one.

Running Bert will send off a cascade of all test files found by the suite executor. To stop Bert, enter ctrl-c; this will stop all further execution, and begin a cleanup step.


## Adding Test Cases

A number of test cases have already been defined for the Bert framework. Let's now take a look at how one might create more.


### File Structure
The Bert Scenarios are set up with the following structure:

```
|-- bert-scenarios
|   |-- tests
|       | -- suite
|            | -- test0
|            | -- test1
|            | -- ...
|            | -- testn
|                 | -- resource0
|                 | -- resource1
|                 | -- ...
```

### Suites
Each Bert tests belong to a specified suite. Suites simple encompass a number of tests that all server a similar purpose or origination. Suites already implemented include legacy, deployment, and stress.

When creating new test scenarios, one should consider if any of the previously created suites already suite their needs. Check each suite folder for a readme on its general purpose, and utility.


#### Creating a Suite
If no suitable suite is found for your test, you may create your own.


##### Executors
Firstly, an executor must be created.

```
|-- bert-scenarios
|   |-- executors
|       | -- legacy.bert
|       | -- deployment.bert
|       | -- ...
```

Executors are Bert files that are run to execute all bert files within a suite. Executors are found in the bert-scenarios/executors folder and are formatted (generally) as follows:

```
suite asdf {
  cd("tests/test")
  runErnieFile("fns.bert")
  files: find(".", ".*\.bert")
  foreach(runErnieFile, files)
}
```
Firstly, notice the block definition of the suite. The special keyword suite tells Ernie to group all test results under the suite named asdf. Next come some general semantics we might find useful. We use Bert's cd function to tell bert it wants to look for files under the specified directory. Following, we run the fns.bert file to load in all utility functions needed by the suite's tests. Finally, we both find and run each bert file that is defined within the current directory.

The pattern laid out here seems roundabout and indirect, however, it opens up possibilities for differing test structures that may come of use. Regardless, this pattern is sufficient for most cases, and one should feel free to replicate it nearly perfectly.


##### Test Folder
Next, we must create a folder to house or test cases. Each folder generally has a suite that will execute its contents. Let's create a folder for our asdf suite to execute. In here we will include a README.md describing the suite, a fns.bert for any utility functions, and a test0 directory to hold our first test

```
mkdir tests/asdf
|-- bert-scenarios
|   |-- tests
|       | -- asdf
|            | -- README.md
|            | -- fns.bert
|            | -- test0
|                 | -- resources
|                 | -- test0.bert
```

##### Test Scenario
Let's now look at an effective way to write a test scenario. Firstly, we must consider our utility functions in fns.bert. Here is where most of our logic will be stored, with only test specific logic in the scenario file. Let's consider this to be finished for us, and focus on the scenario.

```
scenario test0 {

  withDirectory(^name)

  testFn("resources/some-resource", "some-specific-parameter")
}
```

Again, similar to the suite, we have a scenario keyword, followed by a name, and a block defining the scenario. Here we change our working directory to test0, so resources may be easily taken from here. Continuing on, we execute our testFn which we presume to be in fns.bert.

It is worth noting the small amount of true logic that is in this file. One should strive to extract as much as possible from their bert scenario files and move it into a share space, enhancing code reuse and reducing the pain of refactoring.
