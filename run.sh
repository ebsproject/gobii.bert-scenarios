#!/bin/bash

#-----------------------------------------------------------------------------#
### Author

#@author: (L.Cook) ljc237@cornell.edu

#-----------------------------------------------------------------------------#
### Set bash

set -e # Abort script at first error, when a command exits with non-zero status
set -u # Attempt to use undefined variable outputs error message, and forces
# an exit

#-----------------------------------------------------------------------------#
### Set default parameters

suite="*"

#-----------------------------------------------------------------------------#
### Set arguments from run

while [ "$#" -gt 0 ]
  do
    case $1 in
      -web_host) web_host="$2"
      ;;
      -web_port) web_port="$2"
      ;;
      -web_user) web_user="$2"
      ;;
      -web_pass) web_pass="$2"
      ;;
      -crop) crop="$2"
      ;;
      -db_host) db_host="$2"
      ;;
      -db_port) db_port="$2"
      ;;
      -db_name) db_name="$2"
      ;;
      -db_user) db_user="$2"
      ;;
      -db_pass) db_pass="$2"
      ;;
      -ssh_host) ssh_host="$2"
      ;;
      -ssh_user) ssh_user="$2"
      ;;
      -ssh_pass) ssh_pass="$2"
      ;;
      -suite) suite="$2"
      ;;
    esac
    shift
done

#-----------------------------------------------------------------------------#
### Request passwords if set to "askme"
# !!! This should be used to hide passwords and keep them from being logged
# !!! into .bash_history

if [ $web_pass = "askme" ]; then
	read -sp "Please enter the gdm user password: " web_pass
fi
echo;

if [ $db_pass = "askme" ]; then
	read -sp "Please enter the database user (appuser) password: " db_pass
fi
echo;

if [ $ssh_pass = "askme" ]; then
	read -sp "Please enter gadm compute node ssh password: " ssh_pass
fi
echo;


#-----------------------------------------------------------------------------#
### Run Bert

java -jar target/gobii-bert-0.1.0.jar \
  assertions.bert preamble.bert util.bert \
  -c "$(printf '! cd("%s")' $(pwd))" \
  -c "! host('$web_host', $web_port, '/gobii-dev/gobii/v1', '$web_user', '$web_pass')" \
  -c "! crop('$crop')" \
  -c "! database('$db_host', $db_port, '$db_name', '$db_user', '$db_pass')" \
  -c "! shell('$ssh_host', '$ssh_user', '$ssh_pass')" \
  -c "foreach(runErnieFile, find('executors', '$suite.bert'))"
echo;

#-----------------------------------------------------------------------------#
### Exiting

echo "Script has completed.  Good Bye."
echo;
